﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class CommunicationMethodMap : ClassMap<CommunicationMethod>
    {
        public CommunicationMethodMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Frequency);
            Map(x => x.Units);
            HasManyToMany<Customer>(x => x.Customers)
                .Table("CustomerToCommunicationMethod").Schema("videostore").Inverse().Cascade.All();
        }
    }
}
