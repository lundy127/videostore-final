﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Context;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using System.Data.SqlClient;

namespace Mappings
{
    public class SessionFactory
    {
        public static ISessionFactory CreateSessionFactory()
        {
            var builder = new SqlConnectionStringBuilder();
            if (System.Environment.GetEnvironmentVariables().Contains("SERVER_392"))
            {
                builder["server"] = System.Environment.GetEnvironmentVariable("SERVER_392");
            }
            else
            {
                builder["server"] = "(LocalDB)\\MSSQLLocalDB";
            }

            builder["Integrated Security"] = false;     
            
            builder["Initial Catalog"] = System.Environment.GetEnvironmentVariable("USERNAME_392");
            builder["User Id"] = System.Environment.GetEnvironmentVariable("USERNAME_392");
            builder["Password"] = System.Environment.GetEnvironmentVariable("PASSWORD_392");

            return Fluently.Configure()
              .Database(
                MsSqlConfiguration.MsSql2012
                  .ConnectionString(builder.ConnectionString)
                  .DefaultSchema("videostore")
                )
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<AreaMap>())
                .ExposeConfiguration(x => x.CurrentSessionContext<ThreadLocalSessionContext>())
                .ExposeConfiguration(x => x.SetProperty("show_sql", "true"))
                .BuildSessionFactory();
        }
    }
}
