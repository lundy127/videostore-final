﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class RentalMap : ClassMap<Rental>
    {
        public RentalMap()
        {
            Id(x => x.Id);
            Map(x => x.RentalDate);
            Map(x => x.DueDate);
            Map(x => x.ReturnDate);
            References<Customer>(x => x.Customer, "Customer_id").Cascade.All();
            References<Video>(x => x.Video, "Video_id").Cascade.All();
            References<Rating>(x => x.Rating, "Rating_id").Cascade.All();
        }
    }
}
