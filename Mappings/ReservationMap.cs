﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Model;

namespace Mappings
{
    public class ReservationMap : ClassMap<Reservation>
    {
        public ReservationMap()
        {
            Id(x => x.Id);
            References<Customer>(x => x.Customer, "Customer_id").Unique();
            References<Movie>(x => x.Movie, "Movie_id");
            Map(x => x.ReservationDate, "ReservationDate");
        }
    }
}
