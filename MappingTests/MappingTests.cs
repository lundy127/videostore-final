﻿using NHibernate;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Mappings;
using FluentNHibernate.Testing;
using VideoStore.Utilities;
using System.Collections;

namespace MappingTests
{
    public class MappingTests
    {
        private ISession _session;
        private Movie starWars;
        private Movie hoosiers;
        private int custId;
        private int custId2;
        private int employeeId;
        private string zipcodeId;
        private Employee supervisor;
                
        [SetUp]
        public void CreateSession ()
        {
            _session = SessionFactory.CreateSessionFactory().GetCurrentSession();
            _session.CreateSQLQuery("delete from videostore.Rental").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Reservation").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Employee").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Area").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.CommunicationMethod").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Customer").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Video").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.Store").ExecuteUpdate();
            _session.CreateSQLQuery("delete from videostore.ZipCode").ExecuteUpdate();
            _session.CreateSQLQuery("delete from imdb.Genres where Genre = 'newGenre'").ExecuteUpdate();


            starWars = _session.Load<Movie>("tt0076759 ");
            var title = starWars.Title;

            hoosiers = _session.Load<Movie>("tt0091217 ");
            title = hoosiers.Title;
            Name mcFall = new Name
            {
                Title = "Dr.",
                First = "Ryan",
                Middle = "L",
                Last = "McFall",
                Suffix = "Sr"
            };
            Customer cust = new Customer
            {
                Name = mcFall,
                EmailAddress = "mcfall@hope.edu",
                StreetAddress = "27 Graves Place VWF 220",
                Password = "Abc123$!",
                Phone = "616-395-7952",
                ZipCode = new ZipCode
                {
                    Code = "2314",
                    City = "Test Town",
                    State = "PA"
                }
            };
            _session.Save(cust);
            custId = cust.Id;
            Customer testCustomer = new Customer()
            {
                Name = new Name()
                {
                    Title = "Miss.",
                    First = "Claire",
                    Middle = "L",
                    Last = "Lundy",
                    Suffix = ""
                },
                EmailAddress = "claire.lundy@hope.edu",
                Password = "A1232b!",
                StreetAddress = "123 Test St",
                Phone = "1234567890",
                ZipCode = new ZipCode()
                {
                    City = "Holland",
                    State = "Michigan",
                    Code = "7777"
                }
            };
            _session.Save(testCustomer);
            custId2 = testCustomer.Id;
            ZipCode testZipCode = new ZipCode()
            {
                Code = "49425",
                City = "Holland",
                State = "Michigan"
            };
            zipcodeId = testZipCode.Code;
            _session.Save(testZipCode);

            _session.Flush();
            _session.BeginTransaction();
            _session.CreateSQLQuery("set identity_insert videostore.Store on").ExecuteUpdate();
            _session.CreateSQLQuery("insert into videostore.Store(Id, StreetAddress, PhoneNumber, ZipCode, Name) " +
                $"values (0, '123 test st', '1234356745', {zipcodeId}, 'Test Store')").ExecuteUpdate();
            _session.CreateSQLQuery("set identity_insert videostore.Store off").List();

            _session.CreateSQLQuery("set identity_insert videostore.Employee on").ExecuteUpdate();
            _session.CreateSQLQuery("insert into videostore.Employee(Id, Store_id, TopEmployee, First, Last, DateOfBirth, DateHired, Username, Password)" +
                " values (0, 0, 1, 'Ryan', 'McFall', '04/07/1979', '12/10/2021', 'ryan.mcfall', '!232Abwer')").ExecuteUpdate();
            _session.CreateSQLQuery("set identity_insert videostore.Employee off").List();
            _session.GetCurrentTransaction().Commit();

            supervisor = _session.Load<Employee>(0);
            Console.WriteLine(supervisor.Name);
        }

        [Test]
        public void ZipCodeMappingIsCorrect()
        {
            new PersistenceSpecification<ZipCode>(_session)
                .CheckProperty(z => z.Code, "49464")
                .CheckProperty(z => z.City, "Zeeland")
                .CheckProperty(z => z.State, "Michigan");
        }

        [Test]
        public void AreaMappingIsCorrect()
        {
            var zipCodes = new List<ZipCode>
            {
                new ZipCode {
                    Code = "49423",
                    City = "Holland",
                    State = "Michigan"
                },
                new ZipCode
                {
                    Code = "48444",
                    City = "Imlay City",
                    State = "Michigan"
                }
            };

            new PersistenceSpecification<Area>(_session, new DateEqualityComparer())
                .CheckProperty(a => a.Name, "Zeeland")                
                .CheckBag(a => a.ZipCodes, zipCodes)
                .VerifyTheMappings();
        }
                       
        [Test]
        public void CustomerMappingIsCorrect()
        {
            var zeeland = new ZipCode
            {
                Code = "49464",
                City = "Zeeland",
                State = "Michigan"
            };

            var stores = new List<Store>
            {
                new Store
                {
                    StoreName = "Store 2",
                    StreetAddress = "1234 Winterwood Lane",
                    PhoneNumber = "616-748-9715",
                    ZipCode = zeeland
                },
                new Store
                {
                    StoreName = "Store 1",
                    StreetAddress = "1260 Winterwood Lane",
                    PhoneNumber = "616-123-4567",
                    ZipCode = zeeland
                },
            };
            ZipCode code = new ZipCode()
            {
                Code = "49423",
                City = "Holland",
                State = "Michigan"
            };
            Customer cust = _session.Load<Customer>(custId);
            Rental rent1 = new Rental
            {
                Video = new Video
                {
                    Movie = starWars,
                    Store = stores[0]
                },
                Customer = cust,
                DueDate = DateTime.Now,
                RentalDate = DateTime.Now.AddDays(-10),
                ReturnDate = DateTime.Now.AddDays(-7),
                Rating = new Rating
                {
                    Score = 2,
                    Comment = "bad"
                }
            };
            Rental rent2 = new Rental
            {
                Video = new Video
                {
                    Movie = hoosiers,
                    Store = stores[0]
                },
                Customer = cust,
                DueDate = DateTime.Now,
                RentalDate = DateTime.Now.AddDays(-5),
                ReturnDate = DateTime.Now.AddDays(-4),
                Rating = new Rating
                {
                    Score = 3,
                    Comment = "Okay"
                }
            };
            _session.Save(rent1);
            _session.Save(rent2);
            Reservation res = new Reservation
            {
                Movie = starWars,
                Customer = cust,
                ReservationDate = DateTime.Now
            };
            _session.Save(res);
            _session.Evict(cust);
            Customer cust1 = _session.Load<Customer>(custId);
            Assert.AreEqual(rent1, cust1.Rentals[0]);
            Assert.AreEqual(res, cust1.Reservation);
        }
        [Test]
        public void TestCustomerMapping2()
        {
            var zeeland = new ZipCode
            {
                Code = "49464",
                City = "Zeeland",
                State = "Michigan"
            };
            var stores = new List<Store>
            {
                new Store
                {
                    StoreName = "Store 2",
                    StreetAddress = "1234 Winterwood Lane",
                    PhoneNumber = "616-748-9715",
                    ZipCode = zeeland
                },
                new Store
                {
                    StoreName = "Store 1",
                    StreetAddress = "1260 Winterwood Lane",
                    PhoneNumber = "616-123-4567",
                    ZipCode = zeeland
                },
            };
            ZipCode code = new ZipCode()
            {
                Code = "49423",
                City = "Holland",
                State = "Michigan"
            };
            var comms = new List<CommunicationMethod>
            {
                new CommunicationMethod
                {
                    Name = "phone",
                    Frequency = 5,
                    Units = CommunicationMethod.TimeUnit.Day
                }
            };
            new PersistenceSpecification<Customer>(_session)
        .CheckProperty(c => c.Name, new Name()
        {
            Title = "Dr.",
            First = "Ryan",
            Middle = "M",
            Last = "McFall",
            Suffix = "Sr"
        })
        .CheckProperty(c => c.EmailAddress, "ryan.mcfall@hope.edu")
        .CheckProperty(c => c.StreetAddress, "27 Graves Place VWF 220")
        .CheckProperty(c => c.Password, "Abc123$!")
        .CheckProperty(c => c.Phone, "616-395-7952")
        .CheckReference(c => c.ZipCode,
            code
        )
        .CheckInverseList(
            c => c.PreferredStores, stores,
            (c, store) => c.AddPreferredStore(store)
        )
        .CheckList(
                c => c.CommunicationTypes, comms,
                (c, comm) => c.Allow(comm)
        )
        .VerifyTheMappings();
        }

        [Test]
        public void StoreMappingIsCorrect()
        {
            var videos = new List<Video>()
            {
                new Video
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now
                },
                new Video
                {
                    Movie = hoosiers,
                    NewArrival = true, 
                    PurchaseDate = DateTime.Now.AddDays(1)
                }
            };
            ZipCode code = _session.Load<ZipCode>(zipcodeId);
            var managers = new List<Employee>()
            {
                _session.Load<Employee>(employeeId)
            };
            new PersistenceSpecification<Store>(_session)
                .CheckProperty(s => s.StoreName, "Blockbusted")
                .CheckProperty(s => s.StreetAddress, "1234 Broke Street")
                .CheckProperty(s => s.PhoneNumber, "616-666-0000")
                .CheckReference(s => s.ZipCode, 
                    new ZipCode() {
                        Code = "49423",
                        City = "Holland",
                        State = "Michigan"
                    }
                )
                .CheckInverseList( s => s.Videos, videos, (s, v) => s.AddVideo(v))
                //.CheckInverseList( s => s.Managers, managers, (s, m) => s.AddManager(m))
                .VerifyTheMappings();
        }

        [Test]
        public void VideoMappingIsCorrect()
        {
            new PersistenceSpecification<Video>(_session, new DateEqualityComparer())
                .CheckProperty(v => v.NewArrival, true)
                .CheckProperty(v => v.PurchaseDate, DateTime.Now)
                .CheckReference(v => v.Store, 
                    new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "49423",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }
                )
                .CheckReference(v => v.Movie, starWars)
                .VerifyTheMappings();
        }

        [Test]
        public void MovieMappingIsCorrect()
        {
            var genres = new List<Genre>()
            {
                new Genre
                {
                    Name = "Action"
                },
                new Genre
                {
                    Name = "Sci-fi"
                }
            };
            Assert.AreEqual("tt0076759 ", starWars.TitleId);
            Assert.AreEqual("Star Wars: Episode IV - A New Hope", starWars.Title);
            Assert.AreEqual("Star Wars", starWars.OriginalTitle);
            Assert.AreEqual(1977, starWars.Year);
            Assert.AreEqual(121, starWars.RunningTimeInMinutes);
            Assert.AreEqual("PG", starWars.Rating);
            Assert.AreEqual("Action", starWars.PrimaryGenre.Name);
            Assert.AreEqual(3, starWars.Genres.Count);
            Customer cust1 = _session.Load<Customer>(custId);
            Customer cust2 = _session.Load<Customer>(custId2);
            Reservation res = new Reservation()
            {
                Movie = starWars,
                Customer = cust1, 
                ReservationDate = DateTime.Now
            };
            Reservation res1 = new Reservation {
                Movie = starWars,
                Customer = cust2,
                ReservationDate = DateTime.Now
            };
            _session.Save(res);
            _session.Save(res1);
            Assert.AreEqual(2, starWars.Reservations.Count);
            Assert.AreEqual(cust1, starWars.Reservations[0].Customer);
            Assert.AreEqual(cust2, starWars.Reservations[1].Customer);

        }
        [Test]
        public void TestGenreMapping()
        {
            new PersistenceSpecification<Genre>(_session)
                .CheckProperty(x => x.Name, "newGenre")
                .VerifyTheMappings();
        }
        [Test]
        public void TestRentalMapping()
        {
            DateTime rentDate = DateTime.Now.AddDays(-14);
            Customer testCustomer = _session.Load<Customer>(custId2);
            new PersistenceSpecification<Rental>(_session, new DateEqualityComparer())
                .CheckProperty(x => x.RentalDate, rentDate)
                .CheckProperty(x => x.DueDate, rentDate.AddDays(14))
                .CheckProperty(x => x.ReturnDate, rentDate.AddDays(7))
                .CheckReference(x => x.Customer, testCustomer)
                .CheckReference(x => x.Video, new Video
                {
                    Movie = starWars,
                    NewArrival = false,
                    PurchaseDate = DateTime.Now,
                    Store = new Store()
                    {
                        StoreName = "Last Store",
                        StreetAddress = "1234 Broke St",
                        PhoneNumber = "616-666-0000",
                        ZipCode = new ZipCode()
                        {
                            Code = "49423",
                            City = "Holland",
                            State = "Michigan"
                        }
                    }

                })
                .CheckReference(x => x.Rating, new Rating
                {
                    Score = 1,
                    Comment = "Terrible"
                })
                .VerifyTheMappings();
        }
        [Test]
        public void TestRatingMapping()
        {
            new PersistenceSpecification<Rating>(_session)
                .CheckProperty(x => x.Score, 4)
                .CheckProperty(x => x.Comment, "Pretty good")
                .VerifyTheMappings();
        }
        [Test]
        public void TestCommunicationMethodMapping()
        {
            new PersistenceSpecification<CommunicationMethod>(_session)
                .CheckProperty(x => x.Name, "Email")
                .CheckProperty(x => x.Frequency, 1)
                .CheckProperty(x => x.Units, CommunicationMethod.TimeUnit.Month)
                .VerifyTheMappings();
        }
        [Test]
        public void TestCommunicationMethodMapping2()
        {
            CommunicationMethod method = new CommunicationMethod
            {
                Name = "email",
                Frequency = 3,
                Units = CommunicationMethod.TimeUnit.Month
            };
            _session.Save(method);
            var methodId = method.Id;
            Customer c1 = new Customer
            {
                Name = new Name()
                {
                    Title = "Mr",
                    First = "Christian",
                    Middle = "J",
                    Last = "Lundy",
                    Suffix = ""
                },
                EmailAddress = "christian.lundy@hope.edu",
                Password = "A1232b!",
                StreetAddress = "123 Test St",
                Phone = "1234567764",
                ZipCode = new ZipCode()
                {
                    City = "Holland",
                    State = "Michigan",
                    Code = "87964"
                }
            };
            c1.Allow(method);
            _session.Save(c1);
            Customer c2 = new Customer
            {
                Name = new Name()
                {
                    Title = "Miss",
                    First = "Sarah",
                    Middle = "E",
                    Last = "Lundy",
                    Suffix = ""
                },
                EmailAddress = "sarah.lundy@hope.edu",
                Password = "A1232b!",
                StreetAddress = "123 Test St",
                Phone = "12345623432",
                ZipCode = new ZipCode()
                {
                    City = "Holland",
                    State = "Michigan",
                    Code = "45622"
                }
            };
            c2.Allow(method);
            _session.Save(c2);
            var c2Id = c2.Id;
            _session.Flush();
            var sameMethod = _session.Load<CommunicationMethod>(methodId);
            Assert.AreEqual(2,sameMethod.Customers.Count);
        }
        [Test]
        public void TestEmployeeMapping()
        {
            new PersistenceSpecification<Employee>(_session, new DateEqualityComparer())
                .CheckProperty(c => c.Name, new Name()
                {
                    Title = "Miss.",
                    First = "Claire",
                    Middle = "L",
                    Last = "Lundy",
                    Suffix = ""
                })
                .CheckProperty(x => x.DateHired, DateTime.Now)
                .CheckProperty(x => x.DateOfBirth, DateTime.Now.AddYears(-22))
                .CheckProperty(x => x.Username, "Claire.Lundy")
                .CheckProperty(x => x.Password, "Abc123$!")
                .CheckReference(x => x.Store, new Store
                {
                    StoreName = "Last Store",
                    StreetAddress = "1234 Broke St",
                    PhoneNumber = "616-666-0000",
                    ZipCode = new ZipCode()
                    {
                        Code = "49423",
                        City = "Holland",
                        State = "Michigan"
                    }
                })
                .CheckReference<Employee>(x => x.Supervisor, supervisor)
                .VerifyTheMappings();
        }
        [Test]
        public void TestReservationMapping()
        {
            ZipCode code = new ZipCode()
            {
                City = "Holland",
                State = "Michigan",
                Code = "7777"
            };
            Customer cust = _session.Load<Customer>(custId);
            new PersistenceSpecification<Reservation>(_session, new DateEqualityComparer())
            .CheckReference(x => x.Customer, cust)
            .CheckReference(x => x.Movie, starWars)
            .CheckProperty(x => x.ReservationDate, DateTime.Now)
            .VerifyTheMappings();
        }
    }
}
