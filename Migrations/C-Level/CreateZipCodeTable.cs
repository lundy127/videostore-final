﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(3)]
    public class ZipCodeMigration : Migration
    {
        public override void Down()
        {
            Delete.Table("ZipCode").InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("ZipCode").InSchema("videostore")
                .WithColumn("Code").AsString().PrimaryKey()
                .WithColumn("City").AsString().NotNullable()
                .WithColumn("State").AsString().NotNullable();
        }
    }
}
