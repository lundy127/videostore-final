﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(6)]
    public class CreateStoreTable : Migration
    {
        public override void Down()
        {
            Delete.Table("Store")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("Store")
                .InSchema("videostore")                
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Name").AsString().NotNullable()
                .WithColumn("StreetAddress").AsString().NotNullable()
                .WithColumn("PhoneNumber").AsString().NotNullable();
        }
    }
}
