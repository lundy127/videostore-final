﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(32)]
    public class AddStoreToEmployee : Migration
    {
        public override void Down()
        {
            Delete.Column("Store_id").FromTable("Employee").InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Employee").InSchema("videostore")
                .AddColumn("Store_id").AsInt32().Nullable();
        }
    }
}
