﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(30)]
    public class AddCustomerToRental : Migration
    {
        public override void Down()
        {
            Delete.Column("Customer_id").FromTable("Rental").InSchema("videostore");
            Delete.Column("Video_id").FromTable("Rental").InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Rental")
                .InSchema("videostore")
                .AddColumn("Customer_id").AsInt32()
                .AddColumn("Video_id").AsInt32();
        }
    }
}
