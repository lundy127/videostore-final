﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(29)]
    public class AddCustomerRentalsTable : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("Rentalid").OnTable("CustomerRentals").InSchema("videostore");
            Delete.Table("CustomerRentals").InSchema("videostore");
            Delete.Column("Reservation_id").FromTable("Customer").InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Customer").InSchema("videostore").AddColumn("Reservation_id").AsInt32().Nullable()
                .AddColumn("Rental_id").AsInt32().Nullable()
                .AddColumn("CommunicationMethod_id").AsInt32().Nullable();
            Create.Table("CustomerRentals")
                .InSchema("videostore")
                .WithColumn("Customer_id").AsInt32()
                .WithColumn("Rental_id").AsInt32().PrimaryKey();
            Create.ForeignKey("Rentalid").FromTable("CustomerRentals")
                .InSchema("videostore").ForeignColumn("Rental_id")
                .ToTable("Rental").InSchema("videostore").PrimaryColumn("Id").OnUpdate(System.Data.Rule.Cascade);
        }
    }
}
