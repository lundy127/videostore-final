﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(38)]
    public class AddForeignKeystToRental : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("VideoToRental").OnTable("Rental").InSchema("videostore");
            Delete.ForeignKey("CustomerToRental").OnTable("Rental").InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("VideoToRental").FromTable("Rental").InSchema("videostore")
                .ForeignColumn("Video_id")
                .ToTable("Video")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDelete(System.Data.Rule.None);
            Create.ForeignKey("CustomerToRental").FromTable("Rental").InSchema("videostore")
                .ForeignColumn("Customer_id")
                .ToTable("Customer")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDelete(System.Data.Rule.None);
        }
    }
}
