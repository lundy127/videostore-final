﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(15)]
    public class CreateZipCodeToAreaTable : Migration
    {
        public override void Down()
        {
            Delete.Table("ZipCodeToArea")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("ZipCodeToArea")
                .InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Area_Id").AsInt32().NotNullable()
                .WithColumn("ZipCode_Id").AsString().NotNullable();
        }
    }
}
