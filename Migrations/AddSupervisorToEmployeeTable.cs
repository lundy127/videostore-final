﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    //[Migration(40)]
    public class AddSupervisorToEmployeeTable : Migration
    {
        public override void Down()
        {
            Delete.Table("SupervisorToEmployee").InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("SupervisorToEmployee").InSchema("videostore")
                .WithColumn("Employee_id").AsInt32()
                .WithColumn("Supervisor_id").AsInt32();
        }
    }
}
