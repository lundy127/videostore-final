﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(27)]
    public class AddReservationTable : Migration
    {
        public override void Down()
        {
            Delete.Table("Reservation").InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("Reservation").InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey().NotNullable()
                .WithColumn("Customer_id").AsInt32()
                .WithColumn("Movie_id").AsString(255)
                .WithColumn("ReservationDate").AsDateTime();
        }
    }
}
