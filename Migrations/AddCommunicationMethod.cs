﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(28)]
    public class AddCommunicationMethod : Migration
    {
        public override void Down()
        {
            Delete.Table("CommunicationMethod").InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("CommunicationMethod").InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Name").AsString(255)
                .WithColumn("Frequency").AsInt32()
                .WithColumn("Units").AsString(255);
        }
    }
}
