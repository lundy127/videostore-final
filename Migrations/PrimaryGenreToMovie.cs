﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(33)]
    public class PrimaryGenreToMovie : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("PrimaryGenreToMovie")
                .OnTable("Movies")
                .InSchema("imdb");
        }

        public override void Up()
        {
            Create.ForeignKey("PrimaryGenreToMovie")
                .FromTable("Movies")
                .InSchema("imdb")
                .ForeignColumn("PrimaryGenre")
                .ToTable("Genres")
                .InSchema("imdb")
                .PrimaryColumn("Genre")
                .OnDeleteOrUpdate(System.Data.Rule.SetNull);
        }
    }
}
