﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(31)]
    public class AddRatingIdtoRental : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("rentalToRating").OnTable("rental").InSchema("videostore");
            Delete.Column("Rating_id").FromTable("Rental").InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Rental")
                .InSchema("videostore")
                .AddColumn("Rating_id").AsInt32();
            Create.ForeignKey("rentalToRating")
                .FromTable("rental")
                .InSchema("videostore")
                .ForeignColumn("Rating_id")
                .ToTable("Rating")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnUpdate(System.Data.Rule.Cascade);
        }
    }
}
