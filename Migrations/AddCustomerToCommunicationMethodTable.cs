﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(35)]
    public class AddCustomerToCommunicationMethodTable : Migration
    {
        public override void Down()
        {
            Delete.UniqueConstraint("CustomerCommunicationMethodUnique").FromTable("CustomerToCommunicationMethod").InSchema("videostore");
            Delete.ForeignKey("ToCommunicationMethod").OnTable("CustomerToCommunicationMethod").InSchema("videostore");
            Delete.ForeignKey("ToCustomer").OnTable("CustomerToCommunicationMethod").InSchema("videostore");
            Delete.Table("CustomerToCommunicationMethod").InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("CustomerToCommunicationMethod").InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Customer_id").AsInt32()
                .WithColumn("CommunicationMethod_id").AsInt32();
            Create.ForeignKey("ToCustomer").FromTable("CustomerToCommunicationMethod").InSchema("videostore")
               .ForeignColumn("Customer_id").ToTable("Customer").InSchema("videostore").PrimaryColumn("Id")
               .OnDeleteOrUpdate(System.Data.Rule.Cascade);
            Create.ForeignKey("ToCommunicationMethod").FromTable("CustomerToCommunicationMethod").InSchema("videostore")
                .ForeignColumn("CommunicationMethod_id").ToTable("CommunicationMethod").InSchema("videostore").PrimaryColumn("Id")
                .OnDeleteOrUpdate(System.Data.Rule.Cascade);
            Create.UniqueConstraint("CustomerCommunicationMethodUnique")
              .OnTable("CustomerToCommunicationMethod")
              .WithSchema("videostore")
              .Columns("Customer_id", "CommunicationMethod_id");
        }
    }
}
