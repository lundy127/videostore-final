﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    //[Migration(39)]
    public class AddManagersToStoreTable : Migration
    {
        public override void Down()
        {
            Delete.Table("StoreToEmployee").InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("StoreToEmployee").InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().NotNullable()
                .WithColumn("Employee_id").AsInt32()
                .WithColumn("Store_id").AsInt32();
            
        }
    }
}
