﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(24)]
    public class AddEmployee : Migration
    {
        public override void Down()
        {
            Delete.Table("Employee").InSchema("videostore");
        }

        public override void Up()
        {
            Create.Table("Employee").InSchema("videostore")
                .WithColumn("Id").AsInt32().Identity().PrimaryKey()
                .WithColumn("Title").AsString(255).Nullable()
                .WithColumn("First").AsString(255)
                .WithColumn("Middle").AsString(255).Nullable()
                .WithColumn("Last").AsString(255)
                .WithColumn("Suffix").AsString(255).Nullable()
                .WithColumn("DateHired").AsDateTime()
                .WithColumn("DateOfBirth").AsDateTime()
                .WithColumn("Username").AsString(255)
                .WithColumn("Password").AsString(255);
        }
    }
}
