﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(14)]
    public class AddForeignKeyVideoToMovie : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("VideoToMovie")
                .OnTable("Video")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("VideoToMovie")
               .FromTable("Video")
               .InSchema("videostore")
               .ForeignColumn("Movie_Id")
               .ToTable("Movies")
               .InSchema("imdb")
               .PrimaryColumn("TitleId")
               .OnDeleteOrUpdate(System.Data.Rule.Cascade);
        }
    }
}
