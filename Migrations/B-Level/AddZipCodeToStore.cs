﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(9)]
    public class AddZipCodeToStore : Migration
    {
        public override void Down()
        {
            Delete.Column("ZipCode")
                .FromTable("Store")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Store")
                .InSchema("videostore")
                .AddColumn("ZipCode").AsString().NotNullable();
        }
    }
}
