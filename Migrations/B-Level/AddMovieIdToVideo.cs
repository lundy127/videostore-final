﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(13)]
    public class AddMovieIdToVideo : Migration
    {
        public override void Down()
        {
            Delete.Column("Movie_Id")
                .FromTable("Video")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Video")
                .InSchema("videostore")
                .AddColumn("Movie_Id").AsCustom("char(10)").NotNullable();
        }
    }
}
