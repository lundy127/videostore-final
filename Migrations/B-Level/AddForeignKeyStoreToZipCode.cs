﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(10)]
    public class AddForeignKeyStoreToZipCode : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("StoreToZipCode")
                .OnTable("Store")
                .InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("StoreToZipCode")
                .FromTable("Store")
                .InSchema("videostore")
                .ForeignColumn("ZipCode")
                .ToTable("ZipCode")
                .InSchema("videostore")
                .PrimaryColumn("Code")
                .OnUpdate(System.Data.Rule.Cascade);
        }
    }
}
