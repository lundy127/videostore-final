﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(41)]
    public class AddSupervisorToEmployee : Migration
    {
        public override void Down()
        {
            Execute.Sql("ALTER TABLE videostore.Employee DROP CONSTRAINT chkTop");
            Delete.ForeignKey("SupervisorToEmployee").OnTable("Employee").InSchema("videostore");
            Delete.Column("Supervisor_id").FromTable("Employee").InSchema("videostore");
            Delete.Column("TopEmployee").FromTable("Employee").InSchema("videostore");
        }

        public override void Up()
        {
            Alter.Table("Employee").InSchema("videostore")
                .AddColumn("TopEmployee").AsBinary().WithDefaultValue(0)
                .AddColumn("Supervisor_id").AsInt32().Nullable();
            Create.ForeignKey("SupervisorToEmployee").FromTable("Employee")
                .InSchema("videostore").ForeignColumn("Supervisor_id")
                .ToTable("Employee").InSchema("videostore").PrimaryColumn("Id")
                .OnDeleteOrUpdate(System.Data.Rule.None);
            Execute.Sql("ALTER TABLE videostore.Employee " +
                "ADD CONSTRAINT chkTop CHECK((TopEmployee = 1 AND Supervisor_id is null) OR (TopEmployee = 0 AND Supervisor_id is not null))");
        }
    }
}
