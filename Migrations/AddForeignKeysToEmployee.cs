﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(34)]
    public class AddForeignKeysToEmployee : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("StoreToEmployee").OnTable("Employee").InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("StoreToEmployee").FromTable("Employee").InSchema("videostore")
                .ForeignColumn("Store_id")
                .ToTable("Store")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDeleteOrUpdate(System.Data.Rule.Cascade);
            /*Create.ForeignKey("SupervisorToEmployee").FromTable("Employee").InSchema("videostore")
                .ForeignColumn("Store_id")
                .ToTable("S")
                .InSchema("videostore")
                .PrimaryColumn("Id");*/
        }
    }
}
