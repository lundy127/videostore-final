﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(36)]
    public class AddForeignKeysToCustomer : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("ReservationToCustomer").OnTable("Customer").InSchema("videostore");
            Delete.ForeignKey("RentalsToCustomer").OnTable("Customer").InSchema("videostore");
            Delete.ForeignKey("CommunicationTypesToCustomer").OnTable("Customer").InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("ReservationToCustomer")
                .FromTable("Customer").InSchema("videostore")
                .ForeignColumn("Reservation_id")
                .ToTable("Reservation")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDeleteOrUpdate(System.Data.Rule.Cascade);
            Create.ForeignKey("RentalsToCustomer")
                .FromTable("Customer").InSchema("videostore")
                .ForeignColumn("Rental_id")
                .ToTable("Rental")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDeleteOrUpdate(System.Data.Rule.Cascade);
            Create.ForeignKey("CommunicationTypesToCustomer")
                .FromTable("Customer").InSchema("videostore")
                .ForeignColumn("CommunicationMethod_id")
                .ToTable("CommunicationMethod")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDeleteOrUpdate(System.Data.Rule.None);

        }
    }
}
