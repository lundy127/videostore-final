﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Migrations
{
    [Migration(37)]
    public class AddForeignKeysToReservation : Migration
    {
        public override void Down()
        {
            Delete.ForeignKey("CustomerToReservation").OnTable("Reservation").InSchema("videostore");
        }

        public override void Up()
        {
            Create.ForeignKey("CustomerToReservation").FromTable("Reservation")
                .InSchema("videostore")
                .ForeignColumn("Customer_id")
                .ToTable("Customer")
                .InSchema("videostore")
                .PrimaryColumn("Id")
                .OnDeleteOrUpdate(System.Data.Rule.None);
        }
    }
}
